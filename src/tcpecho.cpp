/******************************************************************************
 *  This file is part of tcpecho.                                             *
 *                                                                            *
 *  Copyright (C) 2014  Rouven Spreckels  <n3vu0r@nevux.org>                  *
 *                                                                            *
 *  tcpecho is free software: you can redistribute it and/or modify           *
 *  it under the terms of the GNU Affero General Public License version 3 as  *
 *  published by the Free Software Foundation on 19 November 2007.            *
 *                                                                            *
 *  tcpecho is distributed in the hope that it will be useful,                *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU Affero General Public License for more details.                       *
 *                                                                            *
 *  You should have received a copy of the GNU Affero General Public License  *
 *  along with tcpecho.  If not, see <http://www.gnu.org/licenses/>.          *
 ******************************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define TCPECHO_BUFFER_SIZE	4096
#define TCPECHO_EVENTS_SIZE	64

#include <vio/signal.hpp>
#include <vio/tcp.hpp>

#include <iostream>
#include <list>

class echo {
	typedef std::list<echo> list_t;
	static list_t list;
	list_t::iterator that;
	vio::tcp& tcp;
	std::string host;
	std::string port;
	char data[TCPECHO_BUFFER_SIZE];
	void dump(std::string what) {
		std::cout << what << " " << host << " at " << port << "." << std::endl;
	}
	void come(size_t done, size_t) {
		tcp.get();
		tcp.put(&data[0], done);
	}
	void gone() {
		dump("Echo");
		tcp.get(&data[0], sizeof data);
	}
	void closed() {
		tcp.shut();
		dump("Shut");
		drop();
	}
	void failed() {
		dump("Lost");
		drop();
	}
	void drop() {
		list.erase(that);
	}
public:
	static void hosted(vio::tcp& tcp) {
		list.emplace_front(tcp);
		list.front().that = list.begin();
	}
	echo(vio::tcp& tcp) : tcp(tcp) {
		std::tie(host, port) = tcp.info();
		tcp.closed = std::bind(&echo::closed, this);
		tcp.failed = std::bind(&echo::failed, this);
		tcp.get(std::bind(&echo::come, this, vio::_1, vio::_2), nullptr);
		tcp.put(nullptr, std::bind(&echo::gone, this));
		dump("Host");
		tcp.get(&data[0], sizeof data);
	}
	~echo() {
		tcp.drop();
	}
};

echo::list_t echo::list;

int main(int argc, char** argv) {
	if (2 == argc)
		try {
			vio::io::set ioset(TCPECHO_EVENTS_SIZE);
			vio::signal::own();
			vio::signal sigint([](const signalfd_siginfo&) {
				std::cout << std::endl << "Terminated by user." << std::endl;
				vio::io::set::use();
			}, SIGINT);
			vio::tcp tcp(argv[1], echo::hosted);
			vio::io::set::run();
		}
		catch (vio::io::error& error) {
			std::cerr << error.what() << std::endl;
			return 1;
		}
	else
		std::cerr
			<< "tcpecho [PORT]" << std::endl
			<< "  Echo everything from everyone. Try it with telnet." << std::endl
			<< std::endl
			<< "  With no PORT print help and version information and exit." << std::endl
			<< std::endl
			<< "Version: " << PACKAGE_TARNAME << "-" << PACKAGE_VERSION << std::endl
			<< "License: AGPLv3 as of 19 November 2007" << std::endl
			<< "Authors: Rouven Spreckels <" << PACKAGE_BUGREPORT << ">" << std::endl
			<< "Website: <" << PACKAGE_URL << ">" << std::endl
		;
	return 0;
}
