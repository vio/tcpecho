##############################################################################
#  This file is part of tcpecho.                                             #
#                                                                            #
#  Copyright (C) 2014  Rouven Spreckels  <n3vu0r@nevux.org>                  #
#                                                                            #
#  tcpecho is free software: you can redistribute it and/or modify           #
#  it under the terms of the GNU Affero General Public License version 3 as  #
#  published by the Free Software Foundation on 19 November 2007.            #
#                                                                            #
#  tcpecho is distributed in the hope that it will be useful,                #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Affero General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Affero General Public License  #
#  along with tcpecho.  If not, see <http://www.gnu.org/licenses/>.          #
##############################################################################

# PetaLinux
-include apps.common.mk

include Makefile
